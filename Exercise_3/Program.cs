﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var hangMan = new HangmanService();

            
           starter: while (true)
            {
                hangMan.restart();

                Console.WriteLine(hangMan.getDisplay());
                while (true)
                {
                    Console.Write("please enter character: ");
                    var input = Console.ReadLine().ToCharArray();

                    try
                    {
                        var result = hangMan.input(input[0]);
                        switch (result)
                        {
                            case HangmanService.Result.Correct:
                                {
                                    Console.WriteLine(hangMan.getDisplay());
                                    continue;
                                }
                            case HangmanService.Result.Incorrect:
                                {
                                    Console.WriteLine(String.Format("Sorry, you ’re wrong. Remaining {0}", hangMan.getRemainingTry().ToString()));
                                    continue;
                                }
                            case HangmanService.Result.Used:
                                {
                                    Console.WriteLine("you have already tried this character.");
                                    continue;
                                }
                            case HangmanService.Result.GAMEOVER:
                                {
                                    Console.WriteLine("game over.. Remaining = 0");
                                    goto starter;
                                }
                            case HangmanService.Result.GAMESUCCESS:
                                {
                                    Console.WriteLine("Congratulation, you’re win!!!!!!!!");
                                    goto starter;
                                }
                        }
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        goto starter;
                    }


                }

            }
        }
    }
}
