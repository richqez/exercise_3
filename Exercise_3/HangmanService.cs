﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class HangmanService
    {
        public enum Result {Correct,Incorrect, Used , GAMEOVER , GAMESUCCESS };

        string[] wordsList = new string[3]  {"abc", "cater", "godfather"};

        public char[] charQuesion { get; private set; }

        public char[] userAnswer { get; private set; }

        public int remainTry =  10;


        private bool isSuccess()
        {
            for (int i = 0; i < this.charQuesion.Length; i++)
            {
                if (charQuesion[i] != userAnswer[i])
                {
                    return false;
                }
            }
            return true;
        }

        public HangmanService ()
	    {
            restart();
	    }       

        public void restart()
        {
            remainTry = 10;
            var randomNumber = new Random().Next(0,2);
            var word = wordsList[randomNumber];
            charQuesion = new char[word.Length];
            userAnswer = new char[word.Length];
            for (int i = 0; i < word.Length; i++)
			{
			    charQuesion[i] = word[i];
                userAnswer[i] = '_';
			}
        }

        public string getDisplay()
        {
            var result = "";
            for (int i = 0; i < this.userAnswer.Length; i++)
			{
			  result += userAnswer[i] + " " ;
			}
            return result;
        }

        public Result input(char input)
        {
            this.remainTry -= 1;
            
            if (remainTry <= 0)
            {
                return Result.GAMEOVER;
            }

            var result = Array.IndexOf(this.charQuesion, input);
            if (result!= -1)
            {
                var matchChar = this.charQuesion[result];
                var result_inUserAnswer = Array.IndexOf(this.userAnswer, matchChar);
                if (result_inUserAnswer == -1)
                {
                    this.userAnswer[result] = charQuesion[result];
                    if (isSuccess())
                    {
                        return Result.GAMESUCCESS;
                    }
                    else
                    {
                        return Result.Correct;
                    }
                }
                else
                {
                    return Result.Used;
                }
            }
            else
            {
                return Result.Incorrect;
            }
        }



        public int getRemainingTry()
        {
            return this.remainTry;
        }

    }
}
